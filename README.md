# Haven
Standardized infrastructure for running and managing applications.

## Haven Compliancy Checker
Primary concern of Haven is the Haven Compliancy Compliancy Checker. This tool ensures your [Kubernetes](https://kubernetes.io/) cluster is following the Haven standard in a fully automated fashion. In effect being Haven Compliant means you should be able to painlessly migrate entire workloads from one Haven cluster to another, which don't even have to be deployed on the same environment/cloud. Furthermore you may assume that [Common Ground](https://www.commonground.nl/) applications will always be compatible with your Haven environment(s).

See [haven/cli/README.md](cli/README.md) for the Haven CLI tooling which includes the Haven Compliancy Checker.
See ['De standaard' documentation](https://haven.commonground.nl/docs/de-standaard) for more information about the Haven standard.

## Reference Implementations
Assisting in the deployment of Haven we have documented and engineered multiple Reference Implementations for anyone to use. Please feel free to submit a [merge request](https://gitlab.com/commonground/haven/haven/-/merge_requests/new) with new Reference Implementations.

### OpenStack
See [reference/openstack/README.md](reference/openstack/README.md) for instructions on how to deploy Haven to your OpenStack cloud provider.

This is the Reference Implementation we are using internally at VNG Realisatie.

### Others
See [reference](reference) and the ['Aan de slag' documentation](https://haven.commonground.nl/docs/aan-de-slag).

## Addons
Haven ships optional addons you can easily deploy on your cluster to make access and management easier. Make sure you configure the templates where appropriate. For example requests and limits haven been set by benchmarking load on our own clusters with 3 worker nodes each having 4 CPU cores and 8gb RAM.

### Haven Dashboard
You should probably pay special attention to the Haven Dashboard which provides you an option to easily install software. You can easily use the dashboard with the Haven CLI.

See [haven/cli/README.md](haven/cli/README.md) for the Haven CLI tooling which includes the addons manager.

## Documentation
Besides the README's in this reposity please have a look at the online documentation: https://haven.commonground.nl/

The implementation of the docs website can be found at [docs/README.md](docs/README.md).

## Release management
When ready to release a new Haven version (for example "v5.2.0"):

- Make sure to use semantic versioning correctly (Major.Minor.Patch)
- Update VERSION in reference/openstack/haven\_reference\_openstack.sh to the correct tag.
- Update CHANGELOG.md.
- Create a merge request to master.
- Tag the new release with: `git tag v5.2.0 && git push origin v5.2.0`.
- Trigger the manual release jobs to create a new Gitlab release.
- Update the release notes in Project overview > releases with the specific release notes.

## License
Copyright © VNG Realisatie 2019-2021<br />
Licensed under EUPL v1.2
