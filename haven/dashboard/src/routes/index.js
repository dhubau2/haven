// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import NotFoundPage from '../pages/NotFoundPage'
import ListReleasesPage from '../pages/ListReleasesPage'
import AddReleasePage from '../pages/AddReleasePage'
import EditReleasePage from '../pages/EditReleasePage'
import ListRepositoriesPage from '../pages/ListRepositoriesPage'
import AddRepositoryPage from '../pages/AddRepositoryPage'
import EditRepositoryPage from '../pages/EditRepositoryPage'
import AddNamespacePage from '../pages/AddNamespacePage'
import ListNamespacesPage from '../pages/ListNamespacesPage'

const Routes = () => {
  return (
    <Switch>
      <Redirect exact path="/" to="/releases" />
      <Route path="/releases/add" component={AddReleasePage} />
      <Route
        path="/releases/:namespace/:name/edit"
        component={EditReleasePage}
      />
      <Route path="/releases" component={ListReleasesPage} />
      <Route path="/repositories/add" component={AddRepositoryPage} />
      <Route
        path="/repositories/:namespace/:name/edit"
        component={EditRepositoryPage}
      />
      <Route path="/repositories" component={ListRepositoriesPage} />
      <Route path="/namespaces/add" component={AddNamespacePage} />
      <Route path="/namespaces" component={ListNamespacesPage} />
      <Route path="*" component={NotFoundPage} />
    </Switch>
  )
}

export default Routes
