// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package compliancy

import (
	"github.com/Masterminds/semver/v3"
	extensionsclient "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// Config holds the configuration of the checker
type Config struct {
	KubeConfig       *rest.Config
	KubeClient       *kubernetes.Clientset
	KubeServer       *semver.Version
	KubeLatest       string
	ExtensionsClient *extensionsclient.Clientset
	CNCFConfigured   bool
	RunCISChecks     bool
	HostPlatform     Platform
}
