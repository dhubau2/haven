// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package compliancy

import (
	"bytes"
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/tools/remotecommand"
	"k8s.io/kubectl/pkg/scheme"
)

// RunCommandOnPrivilegedPod takes a string as command input and returns the output from
// running that command from a privileged pod, unless this results in an error.
// It's possible to force scheduling of the pod on a master node when needed.
func RunCommandOnPrivilegedPod(config *Config, useMaster bool, failOnExecErr bool, cmd string) (string, error) {
	// Find a master node to work with.
	nodes, err := config.KubeClient.CoreV1().Nodes().List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return "", err
	}

	master := ""
	if useMaster {
		for _, n := range nodes.Items {
			if n.Labels["kubernetes.io/role"] == "master" {
				master = n.Name
				break
			}

			if _, exists := n.Labels["node-role.kubernetes.io/control-plane"]; exists {
				master = n.Name
				break
			}

			if _, exists := n.Labels["node-role.kubernetes.io/master"]; exists {
				master = n.Name
				break
			}

			if _, exists := n.Labels["node.kubernetes.io/master"]; exists {
				master = n.Name
				break
			}
		}
		if master == "" {
			return "", errors.New("Could not find any master node to run privileged pod on")
		}
	}

	// Create privileged pod. See https://miminar.fedorapeople.org/_preview/openshift-enterprise/registry-redeploy/go_client/executing_remote_processes.html.
	app := "hcc-test-" + strings.ToLower(base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(time.Now().Nanosecond())))[:5])
	ns := "default"

	var priv bool = true

	pod, err := config.KubeClient.CoreV1().Pods(ns).Create(context.Background(), &apiv1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name: app,
		},
		Spec: apiv1.PodSpec{
			Containers: []apiv1.Container{
				{
					Name:    app,
					Image:   "busybox",
					Command: []string{"cat"},
					Stdin:   true,
					SecurityContext: &apiv1.SecurityContext{
						Privileged: &priv,
					},
				},
			},
			HostPID:  true,
			NodeName: master,
		},
	}, metav1.CreateOptions{})
	if err != nil {
		return "", err
	}

	defer func() {
		if err := config.KubeClient.CoreV1().Pods(pod.Namespace).Delete(context.Background(), pod.Name, metav1.DeleteOptions{}); err != nil {
			logging.Error("Could not cleanup privileged pod: '%s'", err.Error())
		}
	}()

	// Wait for pod to become ready.
	watcher, err := config.KubeClient.CoreV1().Pods(pod.Namespace).Watch(
		context.Background(),
		metav1.SingleObject(pod.ObjectMeta),
	)
	if err != nil {
		return "", err
	}

	for event := range watcher.ResultChan() {
		switch event.Type {
		case watch.Modified:
			pod = event.Object.(*apiv1.Pod)

			for _, cond := range pod.Status.Conditions {
				if cond.Type == apiv1.PodReady && cond.Status == apiv1.ConditionTrue {
					watcher.Stop()
				}
			}

		default:
			return "", fmt.Errorf("Unexpected event type '%s'", event.Type)
		}
	}

	// Run command.
	req := config.KubeClient.CoreV1().RESTClient().
		Post().
		Resource("pods").
		Namespace(pod.Namespace).
		Name(pod.Name).
		SubResource("exec").
		VersionedParams(&apiv1.PodExecOptions{
			Container: pod.Spec.Containers[0].Name,
			Command:   []string{"nsenter", "--target", "1", "--mount", "--uts", "--ipc", "--net", "--pid", "--", "sh", "-c", cmd},
			Stdin:     false,
			Stdout:    true,
			Stderr:    true,
			TTY:       false,
		}, scheme.ParameterCodec)

	exec, err := remotecommand.NewSPDYExecutor(config.KubeConfig, "POST", req.URL())
	if err != nil {
		return "", err
	}

	var stdout, stderr bytes.Buffer
	err = exec.Stream(remotecommand.StreamOptions{
		Stdin:  nil,
		Stdout: &stdout,
		Stderr: &stderr,
		Tty:    false,
	})
	if err != nil {
		if failOnExecErr {
			return "", err
		}
	}

	output := stdout.String()
	if len(stderr.String()) != 0 {
		output += "\n" + stderr.String()
	}

	return output, nil
}
