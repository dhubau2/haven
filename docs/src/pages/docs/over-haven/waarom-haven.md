---
title: "Waarom Haven"
path: "/docs"
---

Met Haven kun je applicaties eenvoudiger beheren. De generieke laag van Haven heft verschillen in onderliggende infrastructuur op.

Systeembeheerders kunnen verschillende applicaties op dezelfde manier beheren. Softwareontwikkelaars kunnen makkelijker applicaties voor verschillende organisaties ontwikkelen. Organisaties worden wendbaarder, omdat ze eenvoudiger kunnen wisselen van onderliggende (cloud-)infrastructuur.

`youtube:https://www.youtube.com/embed/405lA6tJgzI`

## Eenvoudig beheer

Een systeembeheerder ontvangt één Docker-image in plaats van allerlei verschillende installatieprocedures per applicatie. Hierdoor wordt het een stuk eenvoudiger om veel applicaties tegelijkertijd te beheren. De ontwikkelaar van de applicatie schrijft hiervoor eenmalig een Dockerfile.

## Snel

Installeer binnen enkele minuten applicaties uit bijvoorbeeld de [Common Ground Componentencatalogus](https://componentencatalogus.commonground.nl/). Het is gemakkelijk om verschillende omgevingen aan te maken, voor bijvoorbeeld testen en productie.

## Veilig

Haven maakt het voor systeembeheerders eenvoudiger om applicaties up-to-date te houden. Daarnaast kunnen alle applicaties geautomatiseerd gescand worden op veiligheid. Haven sluit aan bij de best-practices op het gebied van informatiebeveiliging, zowel vanuit de overheid als de industrie.

## Schaalbaar

Haven biedt de mogelijkheid om applicaties eenvoudig te schalen. Wanneer het aantal gebruikers verandert, kan een applicatie automatisch of handmatig worden opgeschaald of afgeschaald.

## Open standaarden

Haven gebruikt wereldwijde open standaarden zoals het [Open Container Initiative](https://opencontainers.org/) (Docker) en [Kubernetes](https://kubernetes.io/). Door Haven te gebruiken profiteren overheden mee van de wereldwijde technologische ontwikkelingen op het gebied van open source software.

&ensp;

---

*[Klik hier](/docs/de-standaard) voor informatie over de standaard!*
