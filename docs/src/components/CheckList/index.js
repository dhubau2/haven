import React from 'react'
import { Collapsible } from '@commonground/design-system'
import { StyledCategoryList, StyledCategoryColumn, StyledCategory, StyledCheckList, StyledCheck, StyledRationale } from './index.styles'

const CheckList = (props) => {
  const categoryKeys = Object.keys(props.checks)
  const oddCategoryKeys = categoryKeys.filter((_, i) => (i % 2) === 0)
  const evenCategoryKeys = categoryKeys.filter((_, i) => (i % 2) === 1)

  return (
    <StyledCategoryList>
      <StyledCategoryColumn>
        {oddCategoryKeys.map((category, i) => (
          <StyledCategory key={`category-${i}`}>
            <h3>{category}</h3>
            <StyledCheckList>
              {props.checks[category].map((check, j) => (
                <StyledCheck key={`check-${j}`}>
                  <Collapsible title={check.label}>
                    <StyledRationale>
                      {check.rationale}
                    </StyledRationale>
                  </Collapsible>
                </StyledCheck>
              ))}
            </StyledCheckList>
          </StyledCategory>
        ))}
      </StyledCategoryColumn>
      <StyledCategoryColumn>
        {evenCategoryKeys.map((category, i) => (
          <StyledCategory key={`category-${i}`}>
            <h3>{category}</h3>
            <StyledCheckList>
              {props.checks[category].map((check, j) => (
                <StyledCheck key={`check-${j}`}>
                  <Collapsible title={check.label}>
                    <StyledRationale>
                      {check.rationale}
                    </StyledRationale>
                  </Collapsible>
                </StyledCheck>
              ))}
            </StyledCheckList>
          </StyledCategory>
        ))}
      </StyledCategoryColumn>
    </StyledCategoryList>
  )
}

export default CheckList
