// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import { ThemeProvider } from 'styled-components/macro'
import { MemoryRouter as Router, Route } from 'react-router-dom'

import theme from '../../theme'
import TabbedRouteContent from './index'

test('tabs render passed child routes', () => {
  const tabs = [
    { to: '/path/a', title: 'goto a' },
    { to: '/path/b', title: 'goto b', exact: true },
    { to: '/path/b/c', title: 'goto c' },
  ]

  const { getByText } = render(
    <Router>
      <ThemeProvider theme={theme}>
        <TabbedRouteContent tabs={tabs}>
          <Route path="/path/a">Page a</Route>
          <Route path="/path/b" exact>
            Page b
          </Route>
          <Route path="/path/b/c">Page c</Route>
        </TabbedRouteContent>
      </ThemeProvider>
    </Router>,
  )

  const tabA = getByText('goto a')
  const tabB = getByText('goto b')
  const tabC = getByText('goto c')

  fireEvent.click(tabA)
  expect(tabA).toHaveClass('active')
  expect(tabB).not.toHaveClass('active')
  expect(tabC).not.toHaveClass('active')
  expect(getByText('Page a')).toBeInTheDocument()

  fireEvent.click(tabB)
  expect(tabA).not.toHaveClass('active')
  expect(tabB).toHaveClass('active')
  expect(tabC).not.toHaveClass('active')
  expect(getByText('Page b')).toBeInTheDocument()

  fireEvent.click(tabC)
  expect(tabA).not.toHaveClass('active')
  expect(tabB).not.toHaveClass('active')
  expect(tabC).toHaveClass('active')
  expect(getByText('Page c')).toBeInTheDocument()
})
