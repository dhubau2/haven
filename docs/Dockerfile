FROM node:14.17.2 AS build

RUN apt-get update && \
    apt-get install -y libgl1-mesa-glx

COPY ./docs/package.json \
     ./docs/package-lock.json \
     /app/

WORKDIR /app

RUN npm install

COPY ./docs /app
COPY ./haven/cli/pkg/compliancy/static/checks.yaml /haven/cli/pkg/compliancy/static/checks.yaml

ENV GATSBY_TELEMETRY_DISABLED=1
RUN ./node_modules/.bin/gatsby build --prefix-paths

# Create version.json
FROM alpine:3.14.0 AS version

RUN apk add --update jq

ARG HAVEN_VERSION=undefined
RUN jq -ncM --arg version $HAVEN_VERSION '{version: $version}' | tee /version.json

# Copy docs to alpine-based nginx container.
FROM nginx:1.21.1-alpine
EXPOSE 8080

RUN adduser -D -u 1001 appuser

RUN touch /var/run/nginx.pid && \
    chown -R appuser /var/run/nginx.pid && \
    chown -R appuser /var/cache/nginx

COPY ./docs/docker/nginx.conf /etc/nginx/nginx.conf
COPY ./docs/docker/default.conf /etc/nginx/conf.d/default.conf

COPY --from=build /app/public /usr/share/nginx/html
COPY --from=version /version.json /usr/share/nginx/html

USER appuser
