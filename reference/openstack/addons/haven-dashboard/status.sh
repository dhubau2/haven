#!/bin/bash

# Copyright © VNG Realisatie 2019-2021
# Licensed under the EUPL

kubectl -n haven-dashboard get pods,ingresses
